// // 1. Функції у програмуванні потрібні для того, щоб зменшити кількість повторюваного коду і зробити програму більш організованою та легше зрозумілою.
// Вони дозволяють зберегти код у відокремлених блоках, які можна використовувати багато разів із різними параметрами.
// Функції також забезпечують можливість згрупувати певний код разом із потрібними операціями, які потрібно виконати з цим кодом.

// // 2. У функцію можна передавати аргументи, щоб зробити її більш універсальною. Аргументи - це значення, які передаються у функцію, і можуть бути використані для виконання різних дій всередині функції.
//  Це дозволяє використовувати одну і ту ж функцію для обробки різних вхідних даних, що зменшує кількість необхідного коду та спрощує роботу з програмою.

// // 3. Оператор return використовується всередині функції для повернення значення з функції до місця, де вона була викликана.
// Він може бути використаний для повернення різних типів даних, таких як числа, рядки, списки та інші.Коли викликається функція, вона виконує всі необхідні дії, а потім використовує оператор return для повернення значення.
// Це значення можна зберегти у змінній та використовувати для подальшої обробки в програмі.

// ex 1

function caclulate() {
    let num1 = +prompt("Введіть the first число:", 5);
    let num2 = +prompt("Введіть the second число:", 10);
    let operation = prompt("Введіть математичну операцію (+, -, *, /):");


while (isNaN(num1) || isNaN(num2)) {
    alert("Будь ласка, введіть числа!");
    num1 = prompt("Введіть the first число:");
    num2 = prompt("Введіть the second число:");
}

    num1 = parseFloat(num1);
    num2 = parseFloat(num2);
    
    switch (operation) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            return "Невірна операція";
    }
    return result;
}

console.log(caclulate());








// let m = Number(prompt("Write first number", 2));
// let n = Number(prompt("Write second number", 5));
// let operator = prompt("Enter a mathematical operator");

// while (isNaN(n) || isNaN(m)) {
//   m = Number(prompt("Write first number", m));
//   n = Number(prompt("Write second number", n));
// }
// function mathOperations(value1, operator, value2) {
//   let res = 0;

//   switch (operator) {
//     case "+":
//       res = value1 + value2;
//       break;
//     case "-":
//       res = value1 - value2;
//       break;
//     case "*":
//       res = value1 * value2;
//       break;
//     case "/":
//       res = value1 / value2;
//       break;
//     case "^":
//       res = x ^ y;
//       break;
//     default:
//       alert("Try again");
//   }
//   return res;
// }

// console.log(mathOperations(m, operator, n));

